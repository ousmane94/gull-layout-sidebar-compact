import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { Router, RouteConfigLoadStart, ResolveStart, RouteConfigLoadEnd, ResolveEnd } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
    animations: [SharedAnimations]
})
export class SigninComponent implements OnInit {
    loading: boolean;
    loadingText: string;
    signinForm: FormGroup;
    user;
    constructor(
        private fb: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
                this.loadingText = 'Loading Dashboard Module...';

                this.loading = true;
            }
            if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
                this.loading = false;
            }
        });

        this.signinForm = this.fb.group({
            matricule: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

   signin() {
        this.loading = true;
        this.loadingText = 'Connexion en cour ...';
        console.log(this.signinForm.value);
        this.auth.signin(this.signinForm.value)
            .subscribe(resp => {
                console.log(resp);
                console.log(resp.headers.get('Authorization'));
                const jwtToken = resp.headers.get('Authorization');
                this.auth.saveToken(jwtToken);
                 if (this.auth.isAdmin()) {
                     localStorage.setItem('isLoggedin', 'true');
                     this.router.navigateByUrl('/dashboard/v1');
                     this.loading = false;
                } else {
                       console.log('verifier votre identification');
                       this.loading = true;
                }
                this.userService.getUtilisateurConnecte(this.signinForm.value.matricule)
                .subscribe(resp2 => {
                    this.user = resp2;
                    // console.log(this.authentification.utilisateur.prenom);
                    localStorage.setItem('utilisateur', this.user.prenom + ' ' + this.user.nom);
                    localStorage.setItem('agence', this.user.agence.codeagence);

                   }, err2 => {
                     console.log(err2);
                   });
            });
  }
    /* signin() {
        this.loading = true;
        this.loadingText = 'Sigining in...';
        this.auth.signin(this.signinForm.value)
            .subscribe(resp => {
                console.log(resp);
                this.router.navigateByUrl('/dashboard/v2');
                this.loading = false;

            });
    } */

}
