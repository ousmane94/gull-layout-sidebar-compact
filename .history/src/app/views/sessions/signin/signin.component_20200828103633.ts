import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { Router, RouteConfigLoadStart, ResolveStart, RouteConfigLoadEnd, ResolveEnd } from '@angular/router';
import { LocalStoreService } from 'src/app/shared/services/local-store.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
    animations: [SharedAnimations]
})
export class SigninComponent implements OnInit {
    loading: boolean;
    loadingText: string;
    signinForm: FormGroup;
    user;
    constructor(
        private fb: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private strore: LocalStoreService,
        private userservice: UserService
    ) { }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
                this.loadingText = 'Loading Dashboard Module...';
                this.loading = true;
            }
            if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
                this.loading = false;
            }
        });

        this.signinForm = this.fb.group({
            matricule: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
   signin() {
        this.loading = true;
        this.loadingText = 'Sigining in...';
        console.log(this.signinForm.value);

        this.auth.signin(this.signinForm.value)
            .subscribe(resp => {
                console.log(resp);
                console.log(resp.headers.get('Authorization'));
                const jwtToken = resp.headers.get('Authorization');
                this.auth.saveToken(jwtToken);
                 if (resp != null) {
                     localStorage.setItem('isLoggedin', 'true');
                     this.loading = false;
                     this.connexion();
                     // this.strore.setItem('profil', 7);
                } else {
                       console.log('verifier votre identification');
                       this.loading = true;

                }

            });

  }


  connexion() {
    this.userservice.getUtilisateurConnecte(this.signinForm.value.matricule)
    .subscribe(resp2 => {
        console.log(resp2);
        this.user = resp2;
        if (this.user.agence == null) {
            this.strore.setItem('utilisateur', this.user.prenom + ' ' + this.user.nom + '  ' + 'Siege');
            this.strore.setItem('profil', this.user.profils.idprofils);
            this.strore.setItem('matricule', this.signinForm.value.matricule);
            this.router.navigateByUrl('/dashboard/v1');

        } else {
            this.strore.setItem('utilisateur', this.user.prenom + ' ' + this.user.nom + '  ' + this.user.agence.adresseagence);
            this.strore.setItem('agence', this.user.agence.codeagence);
            this.strore.setItem('profil', this.user.profils.idprofils);
            this.strore.setItem('matricule', this.signinForm.value.matricule);
            this.router.navigateByUrl('/dashboard/v1');
        }
       }, err2 => {
         console.log(err2);
       });
  }
  /*   signin() {
        this.loading = true;
        this.loadingText = 'Sigining in...';
        this.auth.signin(this.signinForm.value)
            .subscribe(resp => {
                console.log(resp);
                this.router.navigateByUrl('/dashboard/v2');
                this.loading = false;
                this.strore.setItem('profil', 7);

            });
    } */

}
