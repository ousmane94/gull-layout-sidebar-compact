import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';
import { DialogRegionComponent } from '../../localite/region/dialog-region/dialog-region.component';
import { EditeplanificationComponent } from '../editeplanification/editeplanification.component';

@Component({
  selector: 'app-listeplanification',
  templateUrl: './listeplanification.component.html',
  styleUrls: ['./listeplanification.component.scss']
})
export class ListeplanificationComponent implements OnInit {
  pays: any ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private paysService: PaysService) { }


  ngOnInit() {
    this.getPays();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(EditeplanificationComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      pays: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getPays();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(pays) {
    const dialogRef = this.modalService.open(EditeplanificationComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      pays: pays,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getPays();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getPays() {
    this.paysService.listepays()
      .subscribe(data => {
       this.pays = data; // on charge les données
       }
       , err => { console.log(err); });

  }
}
