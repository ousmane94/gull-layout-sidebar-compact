import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProductService } from 'src/app/shared/services/product.service';
import { debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';
import { DialogRegionComponent } from '../../localite/region/dialog-region/dialog-region.component';
import { EditeplanificationComponent } from '../editeplanification/editeplanification.component';
import { PlaningService } from 'src/app/shared/services/planing.service';



@Component({
  selector: 'app-listeplanification',
  templateUrl: './listeplanification.component.html',
  styleUrls: ['./listeplanification.component.scss']
})
export class ListeplanificationComponent implements OnInit {
  agence: any ;
  planification: any ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private planingService: PlaningService) { }


  ngOnInit() {
    this.getplanification();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(EditeplanificationComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      planification: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getplanification();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(planifications) {
    const dialogRef = this.modalService.open(EditeplanificationComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      planification: planifications,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getplanification();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getplanification() {
    this.planingService.listeplaninfication()
      .subscribe(data => {
       this.planification = data; // on charge les données
       console.log(this.planification);

       }
       , err => { console.log(err); });

    /* this.regions = [{
      idregion : 1,
      nomregion : 'Dakar',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }, {
 idregion : 2,
      nomregion : 'Saint-louis',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    },
    {
      idregion : 3,
      nomregion : 'Thies',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }
  ]; */
  }

}
