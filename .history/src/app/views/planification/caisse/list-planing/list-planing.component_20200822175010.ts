import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { EditeplanificationComponent } from '../../editeplanification/editeplanification.component';
import { DialogPlaningComponent } from '../dialog-planing/dialog-planing.component';
import { LocalStoreService } from 'src/app/shared/services/local-store.service';

@Component({
  selector: 'app-list-planing',
  templateUrl: './list-planing.component.html',
  styleUrls: ['./list-planing.component.scss']
})
export class ListPlaningComponent implements OnInit {
  agence: any ;
  planification: any ;
  codeagence: string = this.strore.getItem('agence');

constructor(private toastr: ToastrService, private modalService: NgbModal,
  private strore: LocalStoreService,
  private planingService: PlaningService) { }


  ngOnInit() {
    this.getplanification();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(DialogPlaningComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      planification: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getplanification();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(planifications) {
    const dialogRef = this.modalService.open(DialogPlaningComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      planification: planifications,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getplanification();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getplanification() {
    this.planingService.listeplaninficationAgence(this.codeagence)
      .subscribe(data => {
       this.planification = data; // on charge les données
       console.log(this.planification);

       }
       , err => { console.log(err); });

  }

}
