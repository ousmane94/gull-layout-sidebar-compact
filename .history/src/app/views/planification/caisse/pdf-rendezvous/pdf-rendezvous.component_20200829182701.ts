import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Rendezvous } from 'src/app/classes/Rendezvous';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { LocalStoreService } from 'src/app/shared/services/local-store.service';
import { UserService } from 'src/app/shared/services/user.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


interface DialogData {
  action?: any;
  rendezvous?: any;
}


class Invoice {
  numerorendezvous: number;
  montant: number;
  numeromembre: string;
  heurerendezvous: string;
  agence: string;
  user: string;



  rendezvou: Rendezvous[] = [];
  additionalDetails: string;

  constructor() {
    // Initially one empty product row we will show
    // tslint:disable-next-line:label-position
    this.rendezvou.push(new Rendezvous());

  }
}


@Component({
  selector: 'app-pdf-rendezvous',
  templateUrl: './pdf-rendezvous.component.html',
  styleUrls: ['./pdf-rendezvous.component.scss']
})
export class PdfRendezvousComponent implements OnInit {
  planification: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  rendezvous; urictg; user; urictgcommune;  urictguser;
  utilisateur: any;
  planingSelect: any;
  userSelect: any;

  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder,
    private toastr: ToastrService, private planing: PlaningService,
    private strore: LocalStoreService,
    private userservice: UserService) { }


  ngOnInit() {
    this.rendezvous = this.data.rendezvous;
    if ( this.rendezvous === '') {
      this.urictg = '';
    } else {
      this.urictg = this.rendezvous.planification.idplanification;
      this.urictgcommune = this.rendezvous.planification;
      console.log('sama planification', this.urictg);
      if ( this.rendezvous.utilisateur == null) {
        this.urictguser = null;
      } else {
        this.urictguser = this.rendezvous.utilisateur;
        this.user = this.rendezvous['utilisateur']['idutilisateur'];
        console.log('sama user', this.urictguser);
      }
    }
/*     this.getCommune();
 */   /*  this.getUser(); */
    this.regionForm = this.fb.group({
      idplanification: [this.urictg,  Validators.required],
      idutilisateur: [this.user, Validators.required],
      numerorendezvous: [this.rendezvous.numerorendezvous, Validators.required],
      heurerendezvous: [this.rendezvous.heurerendezvous, Validators.required],
      numeromembre: [this.rendezvous.numeromembre, Validators.required],
      idrendezvous: [this.rendezvous.idrendezvous, Validators.required],
      montant: [this.rendezvous.montant, Validators.required],
      descriptionrendezvous: [this.rendezvous.descriptionrendezvous, Validators.required],
      etat: [this.rendezvous.etat, Validators.required]


    });
  }
  // tslint:disable-next-line:member-ordering
  invoice = new Invoice();
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  generatePDF(action = 'print') {
    // tslint:disable-next-line:prefer-const
    console.log(this.regionForm.value);
        // tslint:disable-next-line:prefer-const
    let docDefinition = {
      content: [
        {
          text: 'Rendez vous ',
          fontSize: 16,
          alignment: 'center',
          color: '#047886'
        },
        {
          text: 'MECZY',
          fontSize: 20,
          bold: true,
          alignment: 'center',
          decoration: 'underline',
          color: 'skyblue'
        },
        {
          text: 'Details Membre',
          style: 'sectionHeader'
        },
        {
          columns: [
            [
              {
                text: `Agent de Credit : ${this.user.prenom}`,
                bold: true
              },
              { text: this.invoice.montant },
              { text: this.invoice.numeromembre },
              { text: this.invoice.agence },
              { text: this.invoice.user },
              { text: this.invoice.numerorendezvous }
            ],
            [
              {
                text: `Date: ${new Date().toLocaleString()}`,
                alignment: 'right'
              },
              {
                text: `N° Rendez-vous : ${this.regionForm.value.numerorendezvous}`,
                alignment: 'right'
              }
            ]
          ]
        },
        {
          text: 'Details Rendezvous',
          style: 'sectionHeader'
        },
        {
          table: {
            headerRows: 1,
            widths: ['*', 'auto', 'auto', 'auto'],
            body: [
              ['Planing', 'Date', 'Heure', 'Montant'],
              // tslint:disable-next-line:max-line-length
              ...this.invoice.rendezvou.map(p => ([this.regionForm.value.descriptionrendezvous, this.urictgcommune.dateplanification, this.regionForm.value.heurerendezvous, (this.regionForm.value.montant).toFixed(2)])),
              [{text: 'Montant Total', colSpan: 3}, {}, {}, this.regionForm.value.montant.toFixed(2)]
            ]
          }
        },
        {
          text: 'Additional Details',
          style: 'sectionHeader'
        },
        {
            text: this.invoice.additionalDetails,
            margin: [0, 0 , 0, 15]
        },
        {
          columns: [
            [{ qr: `${this.urictgcommune.agence.adresseagence}`, fit: '50' }],
            [{ text: 'Signature', alignment: 'right', italics: true}],
          ]
        },
        {
          text: 'Terms and Conditions',
          style: 'sectionHeader'
        },
        {
            ul: [
              'La Banque meczy vous remercie de votre aimable rendezvous ',
             /*  'Warrenty of the product will be subject to the manufacturer terms and conditions.',
              'This is system generated invoice.', */
            ],
        }
      ],
      styles: {
        sectionHeader: {
          bold: true,
          decoration: 'underline',
          fontSize: 14,
          margin: [0, 15, 0, 15]
        }
      }
    };

    if (action === 'print') {
      pdfMake.createPdf(docDefinition).print();

    } else {
      pdfMake.createPdf(docDefinition).print();
    }

  }

  addProduct() {
    this.invoice.rendezvou.push(new Rendezvous());
  }

// tslint:disable-next-line:eofline
}