import { Component, OnInit } from '@angular/core';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


interface DialogData {
  action?: any;
  rendezvous?: any;
}
@Component({
  selector: 'app-pdf-rendezvous',
  templateUrl: './pdf-rendezvous.component.html',
  styleUrls: ['./pdf-rendezvous.component.scss']
})
export class PdfRendezvousComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }


  generatePDF() {
    const docDefinition = {
      header: 'C#Corner PDF Header',
      content: 'Sample PDF generated with Angular and PDFMake for C#Corner Blog'
    };
    pdfMake.createPdf(docDefinition).open();
  }

}
