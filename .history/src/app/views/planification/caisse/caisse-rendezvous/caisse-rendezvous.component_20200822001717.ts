import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { DialogRendezvousComponent } from '../../rendezvous/dialog-rendezvous/dialog-rendezvous.component';
import { DialogcaisseRendezvousComponent } from '../dialogcaisse-rendezvous/dialogcaisse-rendezvous.component';

@Component({
  selector: 'app-caisse-rendezvous',
  templateUrl: './caisse-rendezvous.component.html',
  styleUrls: ['./caisse-rendezvous.component.scss']
})
export class CaisseRendezvousComponent implements OnInit {
  rendezvous: any ;
  constructor(private toastr: ToastrService, private modalService: NgbModal, private planing: PlaningService) { }

    ngOnInit() {
      this.getRendezvous();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DialogcaisseRendezvousComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        rendezvous: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getRendezvous();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(rendezvous) {
      console.log('edit rendez--------', rendezvous);
      const dialogRef = this.modalService.open(DialogcaisseRendezvousComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        rendezvous: rendezvous,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getRendezvous();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getRendezvous() {
      this.planing.listrendezvous()
        .subscribe(data => {
         this.rendezvous = data; // on charge les données
         console.log('liste rendez--------', this.rendezvous);
         }
         , err => { console.log(err); });
    }
 }
