import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanificationRoutingModule } from './planification-routing.module';
import { AddplanificationComponent } from './addplanification/addplanification.component';
import { EditeplanificationComponent } from './editeplanification/editeplanification.component';
import { ListeplanificationComponent } from './listeplanification/listeplanification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { SharedDirectivesModule } from 'src/app/shared/directives/shared-directives.module';
import { ListRendezvousComponent } from './rendezvous/list-rendezvous/list-rendezvous.component';
import { DialogRendezvousComponent } from './rendezvous/dialog-rendezvous/dialog-rendezvous.component';
import { CaisseRendezvousComponent } from './caisse/caisse-rendezvous/caisse-rendezvous.component';
import { DialogcaisseRendezvousComponent } from './caisse/dialogcaisse-rendezvous/dialogcaisse-rendezvous.component';
import { ListPlaningComponent } from './caisse/list-planing/list-planing.component';
import { DialogPlaningComponent } from './caisse/dialog-planing/dialog-planing.component';
import { CreateComponent } from './rendezvous/create/create.component';
import { DiaRendezvousComponent } from './credit/dia-rendezvous/dia-rendezvous.component';
import { LitRendezvousComponent } from './credit/lit-rendezvous/lit-rendezvous.component';
import { PdfRendezvousComponent } from './caisse/pdf-rendezvous/pdf-rendezvous.component';



@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [AddplanificationComponent, EditeplanificationComponent, ListeplanificationComponent, ListRendezvousComponent, DialogRendezvousComponent, CreateComponent, CaisseRendezvousComponent, DialogcaisseRendezvousComponent, ListPlaningComponent, DialogPlaningComponent, DiaRendezvousComponent, LitRendezvousComponent, PdfRendezvousComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxDatatableModule,
    NgbModule,
    ToastrModule,
    SharedComponentsModule,
    SharedDirectivesModule,
    PlanificationRoutingModule
  ],
    // tslint:disable-next-line:max-line-length
    entryComponents: [DialogRendezvousComponent, EditeplanificationComponent, DialogcaisseRendezvousComponent, DialogPlaningComponent, DiaRendezvousComponent, PdfRendezvousComponent]

})
export class PlanificationModule { }
