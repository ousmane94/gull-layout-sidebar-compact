import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanificationRoutingModule } from './planification-routing.module';
import { AddplanificationComponent } from './addplanification/addplanification.component';
import { EditeplanificationComponent } from './editeplanification/editeplanification.component';
import { ListeplanificationComponent } from './listeplanification/listeplanification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { SharedDirectivesModule } from 'src/app/shared/directives/shared-directives.module';
import { ListRendezvousComponent } from './rendezvous/list-rendezvous/list-rendezvous.component';
import { DialogRendezvousComponent } from './rendezvous/dialog-rendezvous/dialog-rendezvous.component';

@NgModule({
  declarations: [AddplanificationComponent, EditeplanificationComponent, ListeplanificationComponent, ListRendezvousComponent, DialogRendezvousComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxDatatableModule,
    NgbModule,
    ToastrModule,
    SharedComponentsModule,
    SharedDirectivesModule,
    PlanificationRoutingModule
  ]
})
export class PlanificationModule { }
