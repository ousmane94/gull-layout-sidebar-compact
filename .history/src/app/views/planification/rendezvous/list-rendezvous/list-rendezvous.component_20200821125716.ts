import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { DialogRendezvousComponent } from '../dialog-rendezvous/dialog-rendezvous.component';




@Component({
  selector: 'app-list-rendezvous',
  templateUrl: './list-rendezvous.component.html',
  styleUrls: ['./list-rendezvous.component.scss']
})
export class ListRendezvousComponent implements OnInit {
  rendezvous: any ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private planing: PlaningService) { }


  ngOnInit() {
    this.getRendezvous();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(DialogRendezvousComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      rendezvous: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getRendezvous();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(rendezvous) {
    console.log('edit rendez--------', rendezvous);
    const dialogRef = this.modalService.open(DialogRendezvousComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      rendezvous: rendezvous,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getRendezvous();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getRendezvous() {
    this.planing.listrendezvous()
      .subscribe(data => {
       this.rendezvous = data; // on charge les données
       console.log('edit rendez--------', rendezvous);

       }
       , err => { console.log(err); });

  }
}
