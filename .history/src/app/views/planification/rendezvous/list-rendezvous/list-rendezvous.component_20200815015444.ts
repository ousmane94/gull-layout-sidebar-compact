import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';
import { DialogRegionComponent } from 'src/app/views/localite/region/dialog-region/dialog-region.component';
import { DialogRendezvousComponent } from '../dialog-rendezvous/dialog-rendezvous.component';
import { PlaningService } from 'src/app/shared/services/planing.service';

@Component({
  selector: 'app-list-rendezvous',
  templateUrl: './list-rendezvous.component.html',
  styleUrls: ['./list-rendezvous.component.scss']
})
export class ListRendezvousComponent implements OnInit {

  rendezvous: any ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private planingService: PlaningService) { }


  ngOnInit() {
    this.getrendezvous();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(DialogRendezvousComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      rendezvous: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getrendezvous();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(rendezvous) {
    const dialogRef = this.modalService.open(DialogRendezvousComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      resndezvous: rendezvous,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getrendezvous();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getrendezvous() {
    this.planingService.listrendezvous()
      .subscribe(data => {
       this.rendezvous = data; // on charge les données
       }
       , err => { console.log(err); });

    /* this.regions = [{
      idregion : 1,
      nomregion : 'Dakar',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }, {
 idregion : 2,
      nomregion : 'Saint-louis',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    },
    {
      idregion : 3,
      nomregion : 'Thies',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }
  ]; */
  }

}
