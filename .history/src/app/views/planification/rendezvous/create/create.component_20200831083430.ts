import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  clientForm: FormGroup;
  trouvee = 0;
  planification: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  rendezvous; urictg; user; urictgcommune;  urictguser;
  utilisateur: any;
  planingSelect: any;
  userSelect: any;

  constructor(private fb: FormBuilder,private toastr: ToastrService, private planing: PlaningService, private userservice: UserService) { }

  ngOnInit() {
    this.clientForm = this.fb.group({
      codeClt: ['', Validators.required]
    });
    this.getCommune();
    this.getUser();
    this.regionForm = this.fb.group({
      idplanification: ['', Validators.required],
      idutilisateur: ['', Validators.required],
      numerorendezvous: ['', Validators.required],
      heurerendezvous: ['', Validators.required],
      numeromembre: ['', Validators.required],
      idrendezvous: ['', Validators.required],
      descriptionrendezvous: ['', Validators.required],
      etat: ['', Validators.required]


    });
  }
  getClient(){
    this.trouvee = 1
  }
  getCommune() {
    this.planing.listeplaninficationActive()
      .subscribe(data => {
       this.planification = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  getUser() {
    this.userservice.listeUtilisateur()
      .subscribe(data => {
       this.utilisateur = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.planingSelect = this.planification.find(planification => planification.idplanification == idcommuneSelect.target.value);
  }
  getUserSelect(idutilisateurSelect) {
    // tslint:disable-next-line:triple-equals
    this.userSelect = this.utilisateur.find(utilisateur => utilisateur.idutilisateur == idutilisateurSelect.target.value);
  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.loadingText = 'Enregistrement en cours...';
    this.regionForm.value.planification = this.planingSelect;
    this.regionForm.value.utilisateur = this.userservice;
    const data = {
      planification: this.planingSelect,
      idrendezvous: this.regionForm.value.idrendezvous,
      numerorendezvous: this.regionForm.value.numerorendezvous,
      numeromembre: this.regionForm.value.numeromembre,
      heurerendezvous: this.regionForm.value.heurerendezvous,
      descriptionrendezvous: this.regionForm.value.descriptionrendezvous,
      utilisateur: this.userSelect,
      etat: this.regionForm.value.etat
      };
      console.log('save rendez vous', data);
       this.planing.saveRendezvous(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Rendezvous enregistré avec succés!', 'Succes!', { timeOut: 5000 });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    


  }
}
