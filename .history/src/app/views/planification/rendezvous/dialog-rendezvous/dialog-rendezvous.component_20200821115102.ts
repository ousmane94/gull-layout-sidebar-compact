import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { PaysService } from 'src/app/shared/services/pays.service';
import { UserService } from 'src/app/shared/services/user.service';



interface DialogData {
  action?: any;
  rendezvous?: any;
}
@Component({
  selector: 'app-dialog-rendezvous',
  templateUrl: './dialog-rendezvous.component.html',
  styleUrls: ['./dialog-rendezvous.component.scss']
})
export class DialogRendezvousComponent implements OnInit {
  planification: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  rendezvous; urictg; user; urictgcommune;
  utilisateur: any;
  planingSelect: any;
  userSelect: any;

  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private planing: PlaningService, private userservice: UserService) { }


  ngOnInit() {
    this.rendezvous = this.data.rendezvous;
    if ( this.rendezvous === '') {
      this.urictg = '';
    } else {
      this.urictg = this.rendezvous['planification']['idplanification'];
      this.urictgcommune = this.rendezvous['planification'];
      console.log('sama planification', this.urictg);
    }
    this.getCommune();
    this.regionForm = this.fb.group({
      idplanification: [this.urictg, Validators.required],
      numerorendezvous: [this.rendezvous.numerorendezvous, Validators.required],
      heurerendezvous: [this.rendezvous.heurerendezvous, Validators.required],
      numeromembre: [this.rendezvous.numeromembre, Validators.required],
      idrendezvous: [this.rendezvous.idrendezvous, Validators.required],
      etat: [this.rendezvous.etat, Validators.required]


    });
  }
  getCommune() {
    this.planing.listeplaninfication()
      .subscribe(data => {
       this.planification = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.communeSelect = this.commune.find(commune => commune.idcommune == idcommuneSelect.target.value);
  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.commune = this.communeSelect;
    const data = {
      commune: this.communeSelect,
      idquartier: this.regionForm.value.idquartier,
      nomquartier: this.regionForm.value.nomquartier,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log(data);

       this.paysService.saveQuartier(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Quartier enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      if ( data.commune == null) {
        data.commune = this.urictgcommune;
      }
       this.paysService.updateQuartier(this.regionForm.value.idquartier, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Quartier modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
