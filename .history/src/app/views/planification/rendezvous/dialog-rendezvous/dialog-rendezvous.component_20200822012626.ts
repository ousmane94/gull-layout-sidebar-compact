import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { PaysService } from 'src/app/shared/services/pays.service';
import { UserService } from 'src/app/shared/services/user.service';



interface DialogData {
  action?: any;
  rendezvous?: any;
}
@Component({
  selector: 'app-dialog-rendezvous',
  templateUrl: './dialog-rendezvous.component.html',
  styleUrls: ['./dialog-rendezvous.component.scss']
})
export class DialogRendezvousComponent implements OnInit {
  planification: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  rendezvous; urictg; user; urictgcommune;  urictguser;
  utilisateur: any;
  planingSelect: any;
  userSelect: any;

  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private planing: PlaningService, private userservice: UserService) { }


  ngOnInit() {
    this.rendezvous = this.data.rendezvous;
    if ( this.rendezvous === '') {
      this.urictg = '';
    } else {
      this.urictg = this.rendezvous['planification']['idplanification'];
      this.urictgcommune = this.rendezvous['planification'];
      console.log('sama planification', this.urictg);
    }
    if ( this.rendezvous.utilisateur == null) {
      this.urictguser = '';
    } else {
      this.urictguser = this.rendezvous['utilisateur'];
      this.user = this.rendezvous['utilisateur']['idutilisateur'];
      console.log('sama user', this.urictguser);
    }
    this.getCommune();
    this.getUser();
    this.regionForm = this.fb.group({
      idplanification: [this.urictg, Validators.required],
      idutilisateur: [this.user, Validators.required],
      numerorendezvous: [this.rendezvous.numerorendezvous, Validators.required],
      heurerendezvous: [this.rendezvous.heurerendezvous, Validators.required],
      numeromembre: [this.rendezvous.numeromembre, Validators.required],
      idrendezvous: [this.rendezvous.idrendezvous, Validators.required],
      descriptionrendezvous: [this.rendezvous.descriptionrendezvous, Validators.required],
      etat: [this.rendezvous.etat, Validators.required]


    });
  }
  getCommune() {
    this.planing.listeplaninficationActive()
      .subscribe(data => {
       this.planification = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  getUser() {
    this.userservice.listeUtilisateur()
      .subscribe(data => {
       this.utilisateur = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.planingSelect = this.planification.find(planification => planification.idplanification == idcommuneSelect.target.value);
  }
  getUserSelect(idutilisateurSelect) {
    // tslint:disable-next-line:triple-equals
    this.userSelect = this.utilisateur.find(utilisateur => utilisateur.idutilisateur == idutilisateurSelect.target.value);
  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.planification = this.planingSelect;
    this.regionForm.value.utilisateur = this.userservice;

    const data = {
      planification: this.planingSelect,
      idrendezvous: this.regionForm.value.idrendezvous,
      numerorendezvous: this.regionForm.value.numerorendezvous,
      numeromembre: this.regionForm.value.numeromembre,
      heurerendezvous: this.regionForm.value.heurerendezvous,
      descriptionrendezvous: this.regionForm.value.descriptionrendezvous,
      utilisateur: this.userSelect,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log('save rendez vous', data);

       this.planing.saveRendezvous(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Rendezvous enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      if ( data.planification == null) {
        data.planification = this.urictgcommune;
      }
      if ( data.utilisateur == null) {
        data.utilisateur = this.urictguser;
      }
      console.log('*****************', data);

       this.planing.updateRendezvous(this.regionForm.value.idrendezvous, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Rendezvous modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
