import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';
interface DialogData {
  action?: any;
  region?: any;


}
@Component({
  selector: 'app-dialog-rendezvous',
  templateUrl: './dialog-rendezvous.component.html',
  styleUrls: ['./dialog-rendezvous.component.scss']
})
export class DialogRendezvousComponent implements OnInit {
  pays: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  region; urictg;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.region = this.data.region;
    if ( this.region === '') {
      this.urictg = '';
    } else {
      this.urictg = this.region['pays']['idpays'];
    }
    this.getPays();
    this.regionForm = this.fb.group({
      idpays : [this.urictg, Validators.required],
      nomregion : [this.region.nomregion , Validators.required],
      idregion : [this.region.idregion , Validators.required],
      statut: [this.region.statut, Validators.required]


    });
  }
  getPays() {
    this.paysService.listepays()
      .subscribe(data => {
       this.pays = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }

  saveRegion() {
    this.regionForm.value.state = Number(this.regionForm.value.state);
    this.loading = true;
    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      this.toastr.success('Région enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      this.activeModal.close({ confirmed: true });
      // this.service.addCtg(this.paysForm.value)
      //   .subscribe(res => {
      //     this.loading = false;
      //     this.toastr.success('Pays enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      //     this.activeModal.close({ confirmed: true });

      //   },
      //     error => {
      //       console.log('test', error);
      //       this.loading = false;
      //       this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
      //     }
      //   );
    } else {
      this.toastr.success('Région modifié avec succés!', 'Succes!', { timeOut: 5000 });
      this.activeModal.close({ confirmed: true });

      // this.service.putCtg(this.paysForm.value)
      //   .subscribe(res => {
      //     this.loading = false;
      //     this.toastr.success('pays modifié avec succés!', 'Succes!', { timeOut: 5000 });
      //     this.activeModal.close({ confirmed: true });
      //   },
      //     error => {
      //       console.log('test', error);
      //       this.loading = false;
      //       this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

      //     }
      //   );
    }


  }
}
