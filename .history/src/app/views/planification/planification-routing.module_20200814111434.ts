import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddplanificationComponent } from './addplanification/addplanification.component';
import { ForgotComponent } from '../sessions/forgot/forgot.component';
import { EditeplanificationComponent } from './editeplanification/editeplanification.component';
import { ListeplanificationComponent } from './listeplanification/listeplanification.component';
import { ListRendezvousComponent } from './rendezvous/list-rendezvous/list-rendezvous.component';


const routes: Routes = [
  {
    path: 'rendezvous',
    component: ListRendezvousComponent
  },
  {
    path: 'modifier',
    component: EditeplanificationComponent
  },
  {
    path: 'liste',
    component: ListeplanificationComponent
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanificationRoutingModule { }
