import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { PaysService } from 'src/app/shared/services/pays.service';
import { AgenceService } from 'src/app/shared/services/agence.service';


interface DialogData {
  action?: any;
  planification?: any;
}
@Component({
  selector: 'app-editeplanification',
  templateUrl: './editeplanification.component.html',
  styleUrls: ['./editeplanification.component.scss']
})
export class EditeplanificationComponent implements OnInit {
  agences: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  planification; urictg;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private planing: PlaningService, private paysService: PaysService, private agenceService: AgenceService) { }


  ngOnInit() {
    this.planification = this.data.planification;
    if ( this.planification === '') {
      this.urictg = '';
    } else {
      this.urictg = this.planification['agence']['idagence'];
    }
    this.getAgences();
    this.regionForm = this.fb.group({
      idagence: [this.urictg, Validators.required],
      dateplanification: [this.planification.dateplanification, Validators.required],
      maxdossierplanification: [this.planification.maxdossierplanification, Validators.required],
      idplanification: [this.planification.idplanification, Validators.required],
      etat: [this.planification.etat, Validators.required]


    });
  }
  getAgences() {
    this.agenceService.listeAgence()
      .subscribe(data => {
       this.agences = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }

  savePlanification() {
    this.regionForm.value.state = Number(this.regionForm.value.state);
    this.loading = true;
    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      // this.toastr.success('Région enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      // this.activeModal.close({ confirmed: true });
       this.planing.savePlanification(this.regionForm.value)
         .subscribe(res => {
           this.loading = false;
           this.toastr.success('Planing enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });

         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
        );
    } else {
      // this.toastr.success('Région modifié avec succés!', 'Succes!', { timeOut: 5000 });
      // this.activeModal.close({ confirmed: true });
      console.log('test planification', this.regionForm.value);

       this.planing.updatePlanification(this.regionForm.value.idplanification, this.regionForm.value)
         .subscribe(res => {
           this.loading = false;
           this.toastr.success('Planing modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
