import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';

interface DialogData {
  action?: any;
  planification?: any;


}
@Component({
  selector: 'app-editeplanification',
  templateUrl: './editeplanification.component.html',
  styleUrls: ['./editeplanification.component.scss']
})
export class EditeplanificationComponent implements OnInit {
  agences: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  planification; urictg;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.planification = this.data.planification;
    if ( this.planification === '') {
      this.urictg = '';
    } else {
      this.urictg = this.planification['pays']['idpays'];
    }
    this.getAgences();
    this.regionForm = this.fb.group({
      idpays : [this.urictg, Validators.required],
      nomregion : [this.planification.dateplanification, Validators.required],
      idregion : [this.planification.idregion , Validators.required],
      statut: [this.planification.etat, Validators.required]


    });
  }
  getAgences() {
    this.paysService.listeAgence()
      .subscribe(data => {
       this.agences = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }

  savePlanification() {
    this.regionForm.value.state = Number(this.regionForm.value.state);
    this.loading = true;
    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      this.toastr.success('Région enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      this.activeModal.close({ confirmed: true });
      // this.service.addCtg(this.paysForm.value)
      //   .subscribe(res => {
      //     this.loading = false;
      //     this.toastr.success('Pays enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      //     this.activeModal.close({ confirmed: true });

      //   },
      //     error => {
      //       console.log('test', error);
      //       this.loading = false;
      //       this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
      //     }
      //   );
    } else {
      this.toastr.success('Région modifié avec succés!', 'Succes!', { timeOut: 5000 });
      this.activeModal.close({ confirmed: true });

      // this.service.putCtg(this.paysForm.value)
      //   .subscribe(res => {
      //     this.loading = false;
      //     this.toastr.success('pays modifié avec succés!', 'Succes!', { timeOut: 5000 });
      //     this.activeModal.close({ confirmed: true });
      //   },
      //     error => {
      //       console.log('test', error);
      //       this.loading = false;
      //       this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

      //     }
      //   );
    }


  }
}
