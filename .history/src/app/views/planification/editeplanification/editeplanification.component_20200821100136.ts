import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { PaysService } from 'src/app/shared/services/pays.service';
import { AgenceService } from 'src/app/shared/services/agence.service';


interface DialogData {
  action?: any;
  planification?: any;
}
@Component({
  selector: 'app-editeplanification',
  templateUrl: './editeplanification.component.html',
  styleUrls: ['./editeplanification.component.scss']
})
export class EditeplanificationComponent implements OnInit {
  agence: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  planification; urictg;
  urictgcommune;
  agenceSelect: any;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private planing: PlaningService, private paysService: PaysService, private agenceService: AgenceService) { }

  ngOnInit() {
    this.planification = this.data.planification;
    if ( this.planification === '') {
      this.urictg = '';
    } else {
      this.urictg = this.planification['agence']['idagence'];
      this.urictgcommune = this.planification['agence'];
      console.log('sama dept', this.urictg);
    }
    this.getAgence();
    this.regionForm = this.fb.group({
      idagence: [this.urictg, Validators.required],
      nomplanification: [this.planification.nomplanification, Validators.required],
      dateplanification: [this.planification.dateplanification, Validators.required],
     nombredossierplanification: [this.planification.nombredossierplanification, Validators.required],
      maxdossierplanification: [this.planification.maxdossierplanification, Validators.required],
      idplanification: [this.planification.idplanification, Validators.required],
      etat: [this.planification.etat, Validators.required]


    });
  }
  getAgence() {
    this.agenceService.listeAgence()
      .subscribe(data => {
       this.agence = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.agenceSelect = this.agence.find(agence => agence.idagence == idcommuneSelect.target.value);

  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.agence = this.agenceSelect;
    const data = {
      agence: this.agenceSelect,
      idplanification: this.regionForm.value.idplanification,
      nomplanification: this.regionForm.value.nomplanification,
      dateplanification: this.regionForm.value.dateplanification,
      maxdossierplanification: this.regionForm.value.maxdossierplanification,
      nombredossierplanification: this.regionForm.value.nombredossierplanification,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {

       this.planing.savePlanification(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Planing enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      if ( data.agence == null) {
        data.agence = this.urictgcommune;
      }
      console.log('------------', data);

       this.planing.updatePlanification(this.regionForm.value.idplanification, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Planing modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
