import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ListComiteComponent } from './comite/list-comite/list-comite.component';
import { ListAgenceComponent } from './agence/list-agence/list-agence.component';


const routes: Routes = [
  {
    path: 'comite',
    component: ListComiteComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'agence',
    component: ListAgenceComponent
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilisateurRoutingModule { }
