import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { DiologUserComponent } from '../diolog-user/diolog-user.component';
import { DialogRendezvousComponent } from '../../planification/rendezvous/dialog-rendezvous/dialog-rendezvous.component';
import { UserService } from 'src/app/shared/services/user.service';
import { DetailUserComponent } from '../detail-user/detail-user.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  utilisateurs: any ;
  constructor(private toastr: ToastrService, private modalService: NgbModal, private userService: UserService) { }
    ngOnInit() {
      this.getUtilisateurs();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DiologUserComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        utilisateur: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getUtilisateurs();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(utilisateurss) {
      console.log('edit utilisateur--------', utilisateurss);
      const dialogRef = this.modalService.open(DiologUserComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        utilisateur: utilisateurss,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getUtilisateurs();
          }
        }).catch(e => {
          console.log(e);
        });
    }

    goToDialogDetail(utilisateurss) {
      console.log('ajou comite utilisateur--------', utilisateurss);
      const dialogRef = this.modalService.open(DetailUserComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        utilisateur: utilisateurss,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getUtilisateurs();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getUtilisateurs() {
      this.userService.listeUtilisateur()
        .subscribe(data => {
         this.utilisateurs = data; // on charge les données
         console.log('liste utilisateurs--------', this.utilisateurs);
         }
         , err => { console.log(err); });
    }
  }
