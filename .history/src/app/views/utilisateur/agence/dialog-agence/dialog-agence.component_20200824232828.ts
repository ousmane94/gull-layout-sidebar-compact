import { Component, OnInit } from '@angular/core';
import { AgenceService } from 'src/app/shared/services/agence.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { UserService } from 'src/app/shared/services/user.service';
import { PaysService } from 'src/app/shared/services/pays.service';

interface DialogData {
  action?: any;
  agence?: any;
}
@Component({
  selector: 'app-dialog-agence',
  templateUrl: './dialog-agence.component.html',
  styleUrls: ['./dialog-agence.component.scss']
})
export class DialogAgenceComponent implements OnInit {
  comite: any;
  quartier: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  agence; urictg; urictgcomite; urictgquartier; monquartier;
  comiteSelect: any;
  quartierSelect: any;


  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private payservice: PaysService, private userservice: UserService, private agenceService: AgenceService) { }


  ngOnInit() {
    this.agence = this.data.agence;
    if ( this.agence === '') {
      this.urictg = '';
    } else {
      this.monquartier = this.agence['quartier'];
      this.urictgquartier = this.agence['quartier']['idquartier'];
      console.log('sama quartier', this.monquartier);
      if ( this.agence.comite == null) {
        this.urictgcomite = null;
      } else {
        this.urictg = this.agence['comite']['idcomite'];
        this.urictgcomite = this.agence['comite'];
        console.log('sama comite', this.urictgcomite);
      }
    }
    this.getQuartier();
    this.getComite();
    this.regionForm = this.fb.group({
      idcomite: [this.urictg, Validators.required],
      idquartier: [this.urictgquartier, Validators.required],
      idagence: [this.agence.idagence, Validators.required],
      codeagence: [this.agence.codeagence, Validators.required],
      adresseagence: [this.agence.adresseagence, Validators.required],
      etat: [this.agence.etat, Validators.required]


    });
  }
  getQuartier() {
    this.payservice.listeQuartierActive()
      .subscribe(data => {
       this.quartier = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  getComite() {
    this.userservice.listeComiteActive()
      .subscribe(data => {
       this.comite = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.quartierSelect = this.quartier.find(quartier => quartier.idquartier == idcommuneSelect.target.value);
  }
  getUserSelect(idutilisateurSelect) {
    // tslint:disable-next-line:triple-equals
    this.comiteSelect = this.comite.find(comite => comite.idcomite == idutilisateurSelect.target.value);
  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.comite = this.comiteSelect;
    this.regionForm.value.quartier = this.quartierSelect;

    const data = {
      comite: this.comiteSelect,
      idagence: this.regionForm.value.idagence,
      codeagence: this.regionForm.value.codeagence,
      adresseagence: this.regionForm.value.adresseagence,
      quartier: this.quartierSelect,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log('save rendez vous', data);

       this.agenceService.saveAgence(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Agence enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      if ( data.comite == null) {
        data.comite = this.urictgcomite;
      }
      if ( data.quartier == null) {
        data.quartier = this.monquartier;
      }
      console.log('*****************', data);

       this.agenceService.updateAgence(this.regionForm.value.idagence, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Agence modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
