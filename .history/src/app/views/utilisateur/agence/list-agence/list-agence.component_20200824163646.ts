import { Component, OnInit } from '@angular/core';
import { AgenceService } from 'src/app/shared/services/agence.service';
import { DialogAgenceComponent } from '../dialog-agence/dialog-agence.component';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaningService } from 'src/app/shared/services/planing.service';

@Component({
  selector: 'app-list-agence',
  templateUrl: './list-agence.component.html',
  styleUrls: ['./list-agence.component.scss']
})
export class ListAgenceComponent implements OnInit {
  agence: any ;
  // tslint:disable-next-line:max-line-length
  constructor(private toastr: ToastrService, private modalService: NgbModal, private planing: PlaningService, private agenceService: AgenceService) { }
    ngOnInit() {
      this.getAgence();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DialogAgenceComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        agence: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getAgence();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(agences) {
      console.log('edit Agence--------', agences);
      const dialogRef = this.modalService.open(DialogAgenceComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        agence: agences,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getAgence();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getAgence() {
      this.agenceService.listeAgence()
        .subscribe(data => {
         this.agence = data; // on charge les données
         console.log('liste Agence--------', this.agence);
         }
         , err => { console.log(err); });
    }
  }
