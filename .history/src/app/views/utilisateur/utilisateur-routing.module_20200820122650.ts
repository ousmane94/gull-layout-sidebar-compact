import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ListComiteComponent } from './comite/list-comite/list-comite.component';


const routes: Routes = [
  {
    path: 'comite',
    component: ListComiteComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilisateurRoutingModule { }
