import { Component, OnInit } from '@angular/core';
import { DialogComiteComponent } from '../dialog-comite/dialog-comite.component';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-list-comite',
  templateUrl: './list-comite.component.html',
  styleUrls: ['./list-comite.component.scss']
})
export class ListComiteComponent implements OnInit {

  comite: any ;
  constructor(private toastr: ToastrService, private modalService: NgbModal, private userService: UserService) { }

    ngOnInit() {
      this.getComite();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DialogComiteComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        comite: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getComite();
         }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(comite) {
      const dialogRef = this.modalService.open(DialogComiteComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        comite: comite,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getComite();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getComite() {
      this.userService.listeComite()
        .subscribe(data => {
         this.comite = data; // on charge les données
         }
         , err => { console.log(err); });
    }
  }
