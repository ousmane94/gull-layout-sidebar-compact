import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';
import { UserService } from 'src/app/shared/services/user.service';
import { Comite } from 'src/app/classes/Comite';
interface DialogData {
  action?: any;
  comite?: any;
}
@Component({
  selector: 'app-dialog-comite',
  templateUrl: './dialog-comite.component.html',
  styleUrls: ['./dialog-comite.component.scss']
})
export class DialogComiteComponent implements OnInit {

  loading: boolean;
  loadingText: string;
  paysForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  comite: any;
  moncomite: Comite;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private userService: UserService) { }

  ngOnInit() {
    this.comite = this.data.comite;
    this.paysForm = this.fb.group({
      nomcomite: [this.comite.nomcomite, Validators.required],
      idcomite: [this.comite.idcomite, Validators.required],
      typecomite: [this.comite.typecomite, Validators.required],
      numerocomite: [this.comite.numerocomite, Validators.required],
      etat: [this.comite.etat, Validators.required]

    });


  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }

  savePays() {
    // this.paysForm.value.etat = Number(this.paysForm.value.etat);
    this.loading = true;
    this.loadingText = 'Enregistrement en cours...';
    // tslint:disable-next-line:triple-equals
    if (this.data.action == 'add') {
     // this.toastr.success('Pays enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      // this.activeModal.close({ confirmed: true });
      console.log(this.paysForm.value);

      this.userService.saveComite(this.paysForm.value)
         .subscribe(res => {
           this.loading = false;
           this.toastr.success('Comite enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });

         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      // this.toastr.success('Comite modifié avec succés!', 'Succes!', { timeOut: 5000 });
      // this.activeModal.close({ confirmed: true });
      console.log('update comit', this.paysForm.value);
      // tslint:disable-next-line:label-position
      this.moncomite = this.paysForm.value;

       this.userService.updateComite(this.paysForm.value.idcomite, this.moncomite)
         .subscribe(res => {
           this.loading = false;
           this.toastr.success('Comite modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }

}
