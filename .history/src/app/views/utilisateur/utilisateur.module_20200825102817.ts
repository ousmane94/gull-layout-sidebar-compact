import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedDirectivesModule } from 'src/app/shared/directives/shared-directives.module';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { ToastrModule } from 'ngx-toastr';

import { UtilisateurRoutingModule } from './utilisateur-routing.module';
import { UserComponent } from './user/user.component';
import { DiologUserComponent } from './diolog-user/diolog-user.component';
import { ListComiteComponent } from './comite/list-comite/list-comite.component';
import { DialogComiteComponent } from './comite/dialog-comite/dialog-comite.component';
import { ListAgenceComponent } from './agence/list-agence/list-agence.component';
import { DialogAgenceComponent } from './agence/dialog-agence/dialog-agence.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [UserComponent, DiologUserComponent, ListComiteComponent, DialogComiteComponent, DialogAgenceComponent, ListAgenceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxDatatableModule,
    NgbModule,
    ToastrModule,
    SharedComponentsModule,
    SharedDirectivesModule,
    UtilisateurRoutingModule
  ],
    entryComponents: [DiologUserComponent, DialogComiteComponent, DialogAgenceComponent]

})
export class UtilisateurModule { }
