import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/services/user.service';
import { AgenceService } from 'src/app/shared/services/agence.service';
interface DialogData {
  action?: any;
  utilisateur?: any;
}
@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {

  comite: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  utilisateur; urictg;  urictcomite;
  comiteSelect: any;


  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private userService: UserService, private userservice: UserService, private agenceService: AgenceService) { }


  ngOnInit() {
    this.utilisateur = this.data.utilisateur;
    console.log('notre user', this.data.utilisateur);
    // tslint:disable-next-line:triple-equals
    if ( this.utilisateur == '') {
      this.urictg = '';
    }

    this.getComite();

    this.regionForm = this.fb.group({
      matricule: ['', Validators.required],
      comite: ['', Validators.required]
  });

  }

  getComite() {
    this.userService.listeComiteActive()
      .subscribe(data => {
       this.comite = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.comiteSelect = this.comite.find(comite => comite.idcomite == idcommuneSelect.target.value);
  }

  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.comite = this.comiteSelect.nomcomite;
    this.regionForm.value.matricule = this.utilisateur.matricule;


    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log('ajoute comite Utilisateur', this.regionForm.value);

       this.userService.ajouterUtilisateurComite(this.regionForm.value.matricule, this.regionForm.value.comite)
        .subscribe(res => {
           this.loading = false;
           this.activeModal.close({ confirmed: true });
           this.toastr.success('Votre utilisateur a ete bien ajouté  avec succés!', 'Succes!', { timeOut: 5000 });

         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {

      console.log('*****************', this.regionForm.value);

      this.userService.ajouterUtilisateurComite(this.regionForm.value.matricule, this.regionForm.value.comite)
      .subscribe(res => {
           this.loading = false;
           this.toastr.success('Utilisateur modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
