import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListePaysComponent } from './Pays/liste-pays/liste-pays.component';
import { ListeRegionComponent } from './region/liste-region/liste-region.component';
import { ListeDeptComponent } from './departement/liste-dept/liste-dept.component';
import { ListCommuneComponent } from './commune/list-commune/list-commune.component';
import { ListQuartierComponent } from './quartier/list-quartier/list-quartier.component';

const routes: Routes = [
  {
    path: 'pays',
    component: ListePaysComponent
  },
  {
    path: 'region',
    component: ListeRegionComponent
  },
  {
    path: 'dept',
    component: ListeDeptComponent
  },
  {
    path: 'commune',
    component: ListCommuneComponent
  },
  {
    path: 'quartier',
    component: ListQuartierComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocaliteRoutingModule { }
