import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';
interface DialogData {
  action?: any;
  quartier?: any;
}
@Component({
  selector: 'app-dialog-quartier',
  templateUrl: './dialog-quartier.component.html',
  styleUrls: ['./dialog-quartier.component.scss']
})
export class DialogQuartierComponent implements OnInit {

  commune: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  quartier; urictg; urictgcommune;
  communeSelect: any;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.quartier = this.data.quartier;
    if ( this.quartier === '') {
      this.urictg = '';
    } else {
      this.urictg = this.quartier['commune']['idcommune'];
      this.urictgcommune = this.quartier['commune'];
      console.log('sama dept', this.urictgcommune);
    }
    this.getCommune();
    this.regionForm = this.fb.group({
      idcommune: [this.urictg, Validators.required],
      nomquartier: [this.quartier.nomquartier, Validators.required],
      idquartier: [this.quartier.idquartier, Validators.required],
      etat: [this.quartier.etat, Validators.required]


    });
  }
  getCommune() {
    this.paysService.listeCommuneActive()
      .subscribe(data => {
       this.commune = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    this.communeSelect = this.commune.find(commune => commune.idcommune === idcommuneSelect.target.value);
  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.commune = this.communeSelect;
    const data = {
      commune: this.communeSelect,
      idquartier: this.regionForm.value.idquartier,
      nomquartier: this.regionForm.value.nomquartier,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log(data);

       this.paysService.saveQuartier(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Quartier enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      if ( data.commune == null) {
        data.commune = this.urictgcommune;
      }
       this.paysService.updateQuartier(this.regionForm.value.idquartier, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Quartier modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
