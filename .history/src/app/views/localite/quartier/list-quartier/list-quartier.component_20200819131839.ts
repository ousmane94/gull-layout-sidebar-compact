import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';
import { DialogDeptComponent } from '../../departement/dialog-dept/dialog-dept.component';
import { DialogQuartierComponent } from '../dialog-quartier/dialog-quartier.component';

@Component({
  selector: 'app-list-quartier',
  templateUrl: './list-quartier.component.html',
  styleUrls: ['./list-quartier.component.scss']
})
export class ListQuartierComponent implements OnInit {
  quartiers: any ;
  constructor(private toastr: ToastrService, private modalService: NgbModal, private paysService: PaysService) { }

    ngOnInit() {
      this.getQuartier();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DialogQuartierComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        quartier: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            console.log(res);
            this.getQuartier();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(quartiers) {
      const dialogRef = this.modalService.open(DialogQuartierComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        quartier: quartiers,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getQuartier();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getQuartier() {
      this.paysService.listeQuartier()
        .subscribe(data => {
         this.quartiers = data; // on charge les données
         }
         , err => { console.log(err); });
    }
  }
