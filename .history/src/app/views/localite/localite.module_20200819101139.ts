import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocaliteRoutingModule } from './localite-routing.module';
import { ListePaysComponent } from './Pays/liste-pays/liste-pays.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DialogPaysComponent } from './Pays/dialog-pays/dialog-pays.component';
import { SharedDirectivesModule } from 'src/app/shared/directives/shared-directives.module';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { ToastrModule } from 'ngx-toastr';
import { ListeRegionComponent } from './region/liste-region/liste-region.component';
import { DialogRegionComponent } from './region/dialog-region/dialog-region.component';
import { ListeDeptComponent } from './departement/liste-dept/liste-dept.component';
import { DialogDeptComponent } from './departement/dialog-dept/dialog-dept.component';
import { ListCommuneComponent } from './commune/list-commune/list-commune.component';
import { DialogCommuneComponent } from './commune/dialog-commune/dialog-commune.component';
import { ListQuartierComponent } from './quartier/list-quartier/list-quartier.component';
import { DialogQuartierComponent } from './quartier/dialog-quartier/dialog-quartier.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [ListePaysComponent, DialogPaysComponent, ListeRegionComponent, DialogRegionComponent, ListeDeptComponent, DialogDeptComponent, ListCommuneComponent, DialogCommuneComponent, ListQuartierComponent, DialogQuartierComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxDatatableModule,
    NgbModule,
    ToastrModule,
    SharedComponentsModule,
    SharedDirectivesModule,
    LocaliteRoutingModule
  ],
  entryComponents: [DialogPaysComponent, DialogRegionComponent, DialogDeptComponent]

})
export class LocaliteModule { }
