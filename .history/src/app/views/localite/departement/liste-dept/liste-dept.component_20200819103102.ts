import { Component, OnInit } from '@angular/core';

import { DialogDeptComponent } from '../dialog-dept/dialog-dept.component';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';

@Component({
  selector: 'app-liste-dept',
  templateUrl: './liste-dept.component.html',
  styleUrls: ['./liste-dept.component.scss']
})
export class ListeDeptComponent implements OnInit {

  departements: any ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private paysService: PaysService) { }


  ngOnInit() {
    this.getDepartement();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(DialogDeptComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      departement: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          console.log(res);
          this.getDepartement();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(departements) {
    const dialogRef = this.modalService.open(DialogDeptComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      departement: departements,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getDepartement();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getDepartement() {
    this.paysService.listeDepartements()
      .subscribe(data => {
       this.departements = data; // on charge les données
       }
       , err => { console.log(err); });

    /* this.regions = [{
      idregion : 1,
      nomregion : 'Dakar',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }, {
 idregion : 2,
      nomregion : 'Saint-louis',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    },
    {
      idregion : 3,
      nomregion : 'Thies',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }
  ]; */
  }

}
