import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';

interface DialogData {
  action?: any;
  departement?: any;
}
@Component({
  selector: 'app-dialog-dept',
  templateUrl: './dialog-dept.component.html',
  styleUrls: ['./dialog-dept.component.scss']
})
export class DialogDeptComponent implements OnInit {
  region: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  departement; urictg;
  regionSelect: any;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.departement = this.data.departement;
    if ( this.departement === '') {
      this.urictg = '';
    } else {
      this.urictg = this.departement['region']['idregion'];
    }
    this.getRegionActive();
    this.regionForm = this.fb.group({
      idregion: [this.urictg, Validators.required],
      nomdepartement : [this.departement.nomdepartement , Validators.required],
      iddepartement : [this.departement.iddepartement , Validators.required],
      etat: [this.departement.etat, Validators.required]


    });
  }
  getRegionActive() {
    this.paysService.listerRegionsActive()
      .subscribe(data => {
       this.region = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getRegionSelect(idregionSelect) {
    this.regionSelect = this.region.find(region => region.idregion === idregionSelect.target.value);
  }

  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.pays = this.regionSelect;
    const data = {
      region: this.regionSelect,
      iddepartement: this.regionForm.value.iddepartement,
      nomdepartement: this.regionForm.value.nomdepartement,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log(data);

       this.paysService.saveDepartement(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Departement enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
       this.paysService.updateDepartement(this.regionForm.value.iddepartement, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Departement modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
