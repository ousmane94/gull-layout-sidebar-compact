import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';

interface DialogData {
  action?: any;
  pays?: any;
}
@Component({
  selector: 'app-dialog-pays',
  templateUrl: './dialog-pays.component.html',
  styleUrls: ['./dialog-pays.component.scss']
})
export class DialogPaysComponent implements OnInit {

  loading: boolean;
  loadingText: string;
  paysForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  pays: any;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private payservice: PaysService) { }

  ngOnInit() {
    this.pays = this.data.pays;
    this.paysForm = this.fb.group({
      nompays: [this.pays.nompays, Validators.required],
      idpays : [this.pays.idpays, Validators.required],
         etat: [this.pays.etat, Validators.required]

    });


  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }

  savePays() {
    // this.paysForm.value.etat = Number(this.paysForm.value.etat);
    this.loading = true;
    this.loadingText = 'Enregistrement en cours...';
    // tslint:disable-next-line:triple-equals
    if (this.data.action == 'add') {
     // this.toastr.success('Pays enregistré avec succés!', 'Succes!', { timeOut: 5000 });
      // this.activeModal.close({ confirmed: true });
      console.log(this.paysForm.value);

      this.payservice.savePays(this.paysForm.value)
         .subscribe(res => {
           this.loading = false;
           this.toastr.success('Pays enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });

         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      this.toastr.success('Pays modifié avec succés!', 'Succes!', { timeOut: 5000 });
      this.activeModal.close({ confirmed: true });

       this.payservice.updatePays(this.paysForm.value.idpays, this.paysForm.value)
         .subscribe(res => {
           this.loading = false;
           this.toastr.success('pays modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }

}
