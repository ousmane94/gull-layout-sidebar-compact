import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';
interface DialogData {
  action?: any;
  region?: any;
}
@Component({
  selector: 'app-dialog-region',
  templateUrl: './dialog-region.component.html',
  styleUrls: ['./dialog-region.component.scss']
})
export class DialogRegionComponent implements OnInit {
  pays: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  region; urictg;
  paysSelect: any;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.region = this.data.region;
    if ( this.region === '') {
      this.urictg = '';
    } else {
      this.urictg = this.region['pays']['idpays'];
    }
    this.getPays();
    this.regionForm = this.fb.group({
      idpays : [this.urictg, Validators.required],
      nomregion : [this.region.nomregion , Validators.required],
      idregion : [this.region.idregion , Validators.required],
      etat: [this.region.etat, Validators.required]


    });
  }
  getPays() {
    this.paysService.listepaysActive()
      .subscribe(data => {
       this.pays = data; // on charge les données
       console.log(this.pays);

       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getPaysSelect(idpaysSelect) {
    console.log('le idpays ', idpaysSelect.target.value);
    console.log('le idpays ', this.pays);

   // this.paysSelect = this.pays.find(pays => pays.idpays === idpaysSelect.target.value);
   this.paysSelect  = this.pays.filter(pays => pays.idpays === idpaysSelect.target.value);

    console.log('le pays object ', this.paysSelect);

  }

  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.pays = this.paysSelect;
    const data = {
      pays: this.paysSelect,
      idregion: this.regionForm.value.idregion,
      nomregion: this.regionForm.value.nomregion,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log('data ', data);

       this.paysService.saveRegion(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Région enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      console.log(data);

       this.paysService.updateRegion(this.regionForm.value.idregion, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Région modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
