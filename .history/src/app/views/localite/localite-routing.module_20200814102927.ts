import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListePaysComponent } from './Pays/liste-pays/liste-pays.component';
import { ListeRegionComponent } from './region/liste-region/liste-region.component';
import { ListeDeptComponent } from './departement/liste-dept/liste-dept.component';

const routes: Routes = [
  {
    path: 'pays',
    component: ListePaysComponent
  },
  {
    path: 'region',
    component: ListeRegionComponent
  },
  {
    path: 'dept',
    component: ListeDeptComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocaliteRoutingModule { }
