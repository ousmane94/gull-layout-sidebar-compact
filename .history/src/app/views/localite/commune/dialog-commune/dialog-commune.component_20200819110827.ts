import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';
interface DialogData {
  action?: any;
  commune?: any;
}
@Component({
  selector: 'app-dialog-commune',
  templateUrl: './dialog-commune.component.html',
  styleUrls: ['./dialog-commune.component.scss']
})
export class DialogCommuneComponent implements OnInit {

  departement: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  commune; urictg;
  departementSelect: any;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.commune = this.data.commune;
    if ( this.commune === '') {
      this.urictg = '';
    } else {
      this.urictg = this.commune['departement']['iddepartement'];
    }
    this.getDepartementActive();
    this.regionForm = this.fb.group({
      iddepartement: [this.urictg, Validators.required],
      nomcommune: [this.commune.nomregion, Validators.required],
      idcommune: [this.commune.idregion, Validators.required],
      etat: [this.commune.etat, Validators.required]


    });
  }
  getDepartementActive() {
    this.paysService.listeDepartementsActive()
      .subscribe(data => {
       this.commune = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getDepartementSelect(iddepartementSelect) {
    this.departementSelect = this.departement.find(departement => departement.iddepartement === iddepartementSelect.target.value);
  }

  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.departement = this.departementSelect;
    const data = {
      departement: this.departementSelect,
      idcommune: this.regionForm.value.idcommune,
      nomcommune: this.regionForm.value.nomcommune,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log(data);

       this.paysService.saveCommune(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Commune enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
       this.paysService.updateCommune(this.regionForm.value.idcommune, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Commune modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
