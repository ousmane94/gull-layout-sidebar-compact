import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PaysService } from 'src/app/shared/services/pays.service';
import { Commune } from 'src/app/classes/Commune';
interface DialogData {
  action?: any;
  commune?: any;
}
@Component({
  selector: 'app-dialog-commune',
  templateUrl: './dialog-commune.component.html',
  styleUrls: ['./dialog-commune.component.scss']
})
export class DialogCommuneComponent implements OnInit {
  departement: any ;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  commune: Commune; urictg; urictgdept;
  regionSelect;
  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private paysService: PaysService) { }


  ngOnInit() {
    this.commune = this.data.commune;
    console.log( '************', this.commune );

    if ( this.commune) {
      this.urictg = this.commune.departement.iddepartement;
      this.urictgdept = this.commune.departement;
    } else {
      this.urictg = '';

    }
    this.getPays();
    this.regionForm = this.fb.group({
      iddepartement: [this.urictg, Validators.required],
      nomcommune : [this.commune.nomcommune , Validators.required],
      idcommune: [this.commune.idcommune, Validators.required],
      etat: [this.commune.etat, Validators.required]


    });
  }
  getPays() {
    this.paysService.listeDepartementsActive()
      .subscribe(data => {
       this.departement = data; // on charge les données
       console.log(this.departement);

       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getPaysSelect(idpaysSelect) {
    console.log('le iddept ', idpaysSelect.target.value);
    console.log('list dept ', this.departement);

    // tslint:disable-next-line:triple-equals
    this.regionSelect = this.departement.find(departement => departement.iddepartement == idpaysSelect.target.value);
   // this.paysSelect  = this.pays.filter(pays => pays.idpays === idpaysSelect.target.value);

    console.log('le departement object ', this.regionSelect);

  }

  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.departement = this.regionSelect;
    const data = {
      departement: this.regionSelect,
      idcommune: this.regionForm.value.idcommune,
      nomcommune: this.regionForm.value.nomcommune,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log('data ', data);

       this.paysService.saveCommune(data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Commune enregistré avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      console.log(data);
      console.log(data);
      if ( data.departement == null) {
        data.departement = this.urictgdept;
      }
      console.log('apressssss', this.urictgdept);

       this.paysService.updateCommune(this.regionForm.value.idcommune, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Commune modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
