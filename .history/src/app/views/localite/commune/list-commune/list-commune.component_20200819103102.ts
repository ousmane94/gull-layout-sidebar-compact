import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';
import { DialogCommuneComponent } from '../dialog-commune/dialog-commune.component';

@Component({
  selector: 'app-list-commune',
  templateUrl: './list-commune.component.html',
  styleUrls: ['./list-commune.component.scss']
})
export class ListCommuneComponent implements OnInit {

  communes: any ;
  constructor(private toastr: ToastrService, private modalService: NgbModal, private paysService: PaysService) { }

    ngOnInit() {
      this.getCommune();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DialogCommuneComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        commune: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            console.log(res);
            this.getCommune();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(communes) {
      const dialogRef = this.modalService.open(DialogCommuneComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        commune: communes,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getCommune();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getCommune() {
      this.paysService.listeCommune()
        .subscribe(data => {
         this.communes = data; // on charge les données
         }
         , err => { console.log(err); });

    }

  }
