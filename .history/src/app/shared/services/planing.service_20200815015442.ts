import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Planification } from 'src/app/classes/Planification';

@Injectable({
  providedIn: 'root'
})
export class PlaningService {

  jwt: string;
  constructor(private authentification: AuthService, private httpclient: HttpClient , private router: Router) {

  }

  // liste planification
   listeplaninfication() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/pLanification/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste rendezvous
  listrendezvous() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/rendezvous/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }
}
