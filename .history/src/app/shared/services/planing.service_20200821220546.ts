import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Planification } from 'src/app/classes/Planification';
import { Rendezvous } from 'src/app/classes/Rendezvous';
import { Observable} from 'rxjs';
// import {  } from 'src/app/classes/';


@Injectable({
  providedIn: 'root'
})
export class PlaningService {

  jwt: string;
  constructor(private authentification: AuthService, private httpclient: HttpClient , private router: Router) {

  }

  // liste planification planing
   listeplaninfication() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/planification/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste planification planing
  listeplaninficationAgence(codeagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/planification/liste/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
    // liste planification planing active
    listeplaninficationActive() {
      if (this.jwt == null) {
        this.jwt = localStorage.getItem('token');
      }
      // tslint:disable-next-line:max-line-length
      return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/planification/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
    }
      // liste planification planing active par agence
      listeplaninficationAgenceActive(codeagence) {
        if (this.jwt == null) {
          this.jwt = localStorage.getItem('token');
        }
        // tslint:disable-next-line:max-line-length
        return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/planification/active/liste/agence/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
      }
  // liste rendezvous
  listrendezvous() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Rendezvous>( this.authentification.adresse + '/meczy/rendezvous/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }
   // ajouter rendez vous
  public saveRendezvous(rendezvous): Observable<Rendezvous> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Rendezvous>(this.authentification.adresse + '/meczy/rendezvous/create', rendezvous, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // modification rendezvous
  public updateRendezvous(idrendezvous: any, rendezvous: Rendezvous) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/rendezvous/update/' + idrendezvous, rendezvous, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression rendez vous
  public deleteRendezvous(idrendezvous) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/rendezvous/delete/' + idrendezvous, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
    // liste rendezvous  par agence
   getRendezvousAgence(codeagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Rendezvous>( this.authentification.adresse + '/meczy/rendezvous/liste/agence/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste rendezvous  par planification
 getRendezvousPlanification(idplanification) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Rendezvous>( this.authentification.adresse + '/meczy/rendezvous/liste/planification/' + idplanification, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

   // liste rendezvous  par par date et par agence
    getRendezvousDateAgence(date, codeagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Rendezvous>( this.authentification.adresse + '/meczy/rendezvous/liste/date/agence/' + date + '/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
   // ajouter utilisateur
  public savePlanification(planification): Observable<Planification> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Planification>(this.authentification.adresse + '/meczy/planification/create', planification, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Planification
  public updatePlanification(idplanification, planification) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/planification/update/' + idplanification, planification, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression Planification
  public deletePlanification(idplanification) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/planification/delete/' + idplanification, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
   getPlanificationAgence(codeagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/planification/liste/agence/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // liste planification par agence et date
    getPlanificationAgenceDate(codeagence, date) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Planification>( this.authentification.adresse + '/meczy/planification/liste/agence/' + codeagence + '/' + date, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
}
