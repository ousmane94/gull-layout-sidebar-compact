import { Injectable } from '@angular/core';
import { LocalStoreService } from './local-store.service';
import { Router } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import {Host} from '../../classes/Host';
import {Utilisateur} from '../../classes/Utilisateur';
// tslint:disable-next-line:import-spacing
import {JwtHelperService}  from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  [x: string]: any;
  authenticated = true;
  private host: Host = new Host();
  adresse: String = this.host.adresse;
  jwt: string;
  matricule: string;
  prenomuser: string;
  nomUser: string;
  utilisateur: Utilisateur = new Utilisateur();
  private roles: Array<string>;

  authToken: any;
  user: any;
  tokenSubscription = new Subscription();
  timeout;

  constructor(private router: Router, private http: HttpClient, private jwtHelper: JwtHelperService, private store: LocalStoreService) {
    this.checkAuth();

   }


  checkAuth() {
    // this.authenticated = this.store.getItem("demo_login_status");
  }

  getuser() {
    return of({});
  }

saveToken(jwt: string) {
localStorage.setItem('token', jwt);
this.jwt = jwt;
this.parsJWT();
}
parsJWT() {
const jwtHelper = new JwtHelperService();
const objJWt = jwtHelper.decodeToken(this.jwt);
console.log(objJWt);
this.matricule = objJWt.sub;
this.roles = objJWt.profil;
console.log('matricule ' + this.matricule);
console.log('Profils ' + this.roles);
}
isAdmin() {
return this.roles.indexOf('admin') >= 0;
}

isSuperviseur() {
return this.roles.indexOf('supervieur') >= 0;
}
isAgentcredit() {
return this.roles.indexOf('agent credit') >= 0;
}
isDg() {
return this.roles.indexOf('directeur') >= 0;
}
isComite() {
return this.roles.indexOf('comite') >= 0;
}

isAutentecad() {
return this.roles && ( this.isAdmin() || this.isSuperviseur() || this.isDg() || this.isAgentcredit() || this.isComite());

}
loadToken() {
this.jwt = localStorage.getItem('token');
this.parsJWT();
return this.jwt;
}
intiParam() {
this.jwt = undefined;
this.matricule = undefined;
this.roles = undefined;
}

 /*  signin(credentials) {
    this.authenticated = true;
    this.store.setItem("demo_login_status", true);
    //return of({}).pipe(delay(1500));
    return this.http.post(this.adresse + '/login', credentials, { observe: 'response'});

  } */


  storeUserData(token, user) {
    this.timeout = this.jwtHelper.getTokenExpirationDate(token).valueOf() - new Date().valueOf();
    sessionStorage.setItem('id_token', token);
    sessionStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
    this.emit({ matricule: this.user.matricule });
    this.expirationCounter(this.timeout);
  }


  expirationCounter(timeout) {
    this.tokenSubscription.unsubscribe();
    this.tokenSubscription = of(null).pipe(delay(timeout)).subscribe((expired) => {
      console.log('EXPIRED!!');

      this.signout();
      this.router.navigate(['/sessions/signin']);
    });
  }

  signin(credentials) {
    this.authenticated = true;
    this.store.setItem('demo_login_status', true);
    // return of({}).pipe(delay(1500));
     return this.http.post(this.adresse + '/login', credentials, { observe: 'response'});

  }


   signout() {
    this.authenticated = false;
    this.tokenSubscription.unsubscribe();
    this.authToken = null;
    this.user = null;
    sessionStorage.clear();
  }

/*   signout() {
    this.authenticated = false;
    this.store.setItem('demo_login_status', false);
    this.router.navigateByUrl('/sessions/signin');
  } */
}
