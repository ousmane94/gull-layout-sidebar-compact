import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { Pays } from 'src/app/classes/Pays';
import { Region } from 'src/app/classes/Region';
import { Departement } from 'src/app/classes/Departement';
import { Commune } from 'src/app/classes/Commune';
import { Quartier } from 'src/app/classes/Quartier';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaysService {

  jwt: string;
  constructor(private authentification: AuthService, private httpclient: HttpClient , private router: Router) {
  }
  // ajouter Pays
  public savePays(pays): Observable<Pays> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Pays>(this.authentification.adresse + '/meczy/pays/create', pays, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Pays
  public updatePays(idpays: any, pays: Pays) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/pays/update/' + idpays, pays, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

// pour la suppression Pays
  public deletePays(idpays) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/pays/delete/' + idpays, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste pays
   listepays() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length

    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Pays>( this.authentification.adresse + '/meczy/pays/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

    // liste pays active
   listepaysActive() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length

    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Pays>( this.authentification.adresse + '/meczy/pays/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste Region
  listeRegions() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Region>( this.authentification.adresse + '/meczy/region/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste Region active
  listerRegionsActive() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Region>( this.authentification.adresse + '/meczy/region/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // ajouter Region
  public saveRegion(region): Observable<Region> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Region>(this.authentification.adresse + '/meczy/region/create', region, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Region
  public updateRegion(idregion: any, region: Region) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/region/update/' + idregion, region, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression Region
  public deleteRegion(idregion) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/region/delete/' + idregion, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

   // liste Departement
  listeDepartements() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Departement>( this.authentification.adresse + '/meczy/departement/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

     // liste Departement active
  listeDepartementsActive() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Departement>( this.authentification.adresse + '/meczy/departement/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }


  // ajouter Departement
  public saveDepartement(departement): Observable<Departement> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Departement>(this.authentification.adresse + '/meczy/departement/create', departement, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Departement
  public updateDepartement(iddepartement: any, departement: Departement) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/departement/update/' + iddepartement, departement, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression Departement
  public deleteDepartement(iddepartement) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/departement/delete/' + iddepartement, {headers: new HttpHeaders({'authorization': this.jwt})});
  }


 // ajouter Commune
  public saveCommune(commune): Observable<Commune> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Commune>(this.authentification.adresse + '/meczy/commune/create', commune, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Commune
  public updateCommune(idcommune: any, commune: Commune) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/commune/update/' + idcommune, commune, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression Commune
  public deleteCommune(idcommune) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/commune/delete/' + idcommune, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
    // liste Commune
  listeCommune() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Commune>( this.authentification.adresse + '/meczy/commune/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste Commune active
  listeCommuneActive() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Commune>( this.authentification.adresse + '/meczy/commune/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // ajouter Quartier
  public saveQuartier(commune): Observable<Quartier> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Quartier>(this.authentification.adresse + '/meczy/quartier/create', commune, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Quartier
  public updateQuartier(idquartier: any, quartier: Quartier) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/quartier/update/' + idquartier, quartier, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression Quartier
  public deleteQuartier(idquartier) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/quartier/delete/' + idquartier, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

    // liste Quartier
  listeQuartier() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Quartier>( this.authentification.adresse + '/meczy/quartier/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

      // liste Quartier Active
  listeQuartierActive() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Quartier>( this.authentification.adresse + '/meczy/quartier/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }



}
