import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Agence } from 'src/app/classes/Agence';
import {Observable} from 'rxjs';
import { Profils } from 'src/app/classes/Profils';


@Injectable({
  providedIn: 'root'
})
export class AgenceService {

 jwt: string;
  constructor(private authentification: AuthService, private httpclient: HttpClient , private router: Router) {
  }

  // liste Agence
    listeAgence() {
      if (this.jwt == null) {
        this.jwt = localStorage.getItem('token');
      }
      // tslint:disable-next-line:max-line-length
      return this.httpclient.get<Agence>( this.authentification.adresse + '/meczy/agence/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
    }
     // liste Agence
     listeAgenceActive() {
      if (this.jwt == null) {
        this.jwt = localStorage.getItem('token');
      }
      // tslint:disable-next-line:max-line-length
      return this.httpclient.get<Agence>( this.authentification.adresse + '/meczy/agence/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
    }
    // ajouter Agence
  public saveAgence(agence): Observable<Agence> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Agence>(this.authentification.adresse + '/meczy/agence/create', agence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification Agence
  public updateAgence(idagence: any, agence: Agence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/agence/update/' + idagence, agence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression Agence
  public deleteAgence(idagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    console.log(this.jwt);

    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/agence/delete/' + idagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

}
