import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { Pays } from 'src/app/classes/Pays';
import { Region } from 'src/app/classes/Region';
@Injectable({
  providedIn: 'root'
})
export class PaysService {

  jwt: string;
  constructor(private authentification: AuthService, private httpclient: HttpClient , private router: Router) {

  }

  // liste pays
   listepays() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Pays>( this.authentification.adresse + '/meczy/pays/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste Region
  listeRegions() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Region>( this.authentification.adresse + '/meczy/region/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }
    // liste Agence
    listeAgence() {
      if (this.jwt == null) {
        this.jwt = localStorage.getItem('token');
      }
      // tslint:disable-next-line:max-line-length
      return this.httpclient.get<Region>( this.authentification.adresse + '/meczy/agence/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
    }
}
