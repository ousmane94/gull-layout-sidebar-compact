import { Injectable } from '@angular/core';
import { LocalStoreService } from './local-store.service';
import { Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import {Host} from '../../classes/Host';
import {Utilisateur} from '../../classes/Utilisateur';
// tslint:disable-next-line:import-spacing
import {JwtHelperService}  from '@auth0/angular-jwt';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authenticated = true;
  private host: Host = new Host();
  adresse: String = this.host.adresse;
  jwt: string;
  matricule: string;
  prenomuser: string;
  nomUser: string;
  utilisateur: Utilisateur = new Utilisateur();
  private roles: Array<string>;
/* addddddd */
  authToken: any;
  profils: any;
  tokenSubscription = new Subscription();
  timeout;



  constructor(private store: LocalStoreService, private http: HttpClient , private router: Router) {
    this.checkAuth();
  }



  checkAuth() {
    // this.authenticated = this.store.getItem("demo_login_status");
  }

  getuser() {
    return of({});
  }

saveToken(jwt: string) {
localStorage.setItem('token', jwt);
this.jwt = jwt;
this.parsJWT();
}
parsJWT() {
const jwtHelper = new JwtHelperService();
const objJWt = jwtHelper.decodeToken(this.jwt);
console.log(objJWt);
this.matricule = objJWt.sub;
this.roles = objJWt.profil;
console.log('matricule ' + this.matricule);
console.log('Profils ' + this.roles);

    this.timeout = jwtHelper.getTokenExpirationDate(this.jwt).valueOf() - new Date().valueOf();
    sessionStorage.setItem('id_token', this.jwt);
    sessionStorage.setItem('matricule', JSON.stringify(this.matricule));
        sessionStorage.setItem('profils', JSON.stringify(this.roles));
    this.authToken = this.jwt;
    // this.emit({ matricule: this.matricule });
    this.expirationCounter(this.timeout);
       const time_to_login = 10 * 72 * 3600; // one week
        localStorage.setItem('timer', JSON.stringify(time_to_login));

}
expirationCounter(timeout) {
    this.tokenSubscription.unsubscribe();
    this.tokenSubscription = of(null).pipe(delay(timeout)).subscribe((expired) => {
      console.log('EXPIRED!!');
      this.signout();
    });
  }
  connecte(tet) {
    return this.roles.indexOf(tet) >= 0;
    }
isAdmin() {
return this.roles.indexOf('admin') >= 0;
}

isSuperviseur() {
return this.roles.indexOf('supervieur') >= 0;
}
isAgentcredit() {
return this.roles.indexOf('agent credit') >= 0;
}
isDg() {
return this.roles.indexOf('directeur') >= 0;
}
isComite() {
return this.roles.indexOf('comite') >= 0;
}

isAutentecad() {
return this.roles && ( this.isAdmin() || this.isSuperviseur() || this.isDg() || this.isAgentcredit() || this.isComite());

}
loadToken() {
this.jwt = localStorage.getItem('token');
this.parsJWT();
return this.jwt;
}
intiParam() {
this.jwt = undefined;
this.matricule = undefined;
this.roles = undefined;
}

 /*  signin(credentials) {
    this.authenticated = true;
    this.store.setItem("demo_login_status", true);
    //return of({}).pipe(delay(1500));
    return this.http.post(this.adresse + '/login', credentials, { observe: 'response'});

  } */

  signin(credentials) {
    this.authenticated = true;
    this.store.setItem('demo_login_status', true);
    // return of({}).pipe(delay(1500));
     return this.http.post(this.adresse + '/login', credentials, { observe: 'response'});

  }

  signout() {
    this.authenticated = false;
    this.store.setItem('demo_login_status', false);

    this.tokenSubscription.unsubscribe();
    this.authToken = null;
    this.matricule = null;
    this.profils = null;
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigateByUrl('/sessions/signin');
    localStorage.clear();
    sessionStorage.clear();

  }
}
