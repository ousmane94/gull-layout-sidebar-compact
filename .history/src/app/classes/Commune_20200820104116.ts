import { Departement  } from './Departement';
export class Commune {
    constructor() {}
     idcommune: number;
     nomcommune: string;
     etat: number;
     departement: Departement;
}
