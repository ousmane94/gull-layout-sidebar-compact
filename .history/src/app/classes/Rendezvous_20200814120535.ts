import {Pays   } from './Pays';
export class Rendezvous {
    constructor() {}
     idrendezvous: number;
     numerorendezvous: string;
     heurerendezvous: string;
     numeromembre: string;
     descriptionrendezvous: string;
     etat: boolean;
}
