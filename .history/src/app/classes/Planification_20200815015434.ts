import { Agence  } from './Agence';
export class Planification {
    constructor() {}
     idplanification: number;
     dateplanification: string;
     nombredossierplanification: number;
     maxdossierplanification: number;
     agence: Agence;
}
