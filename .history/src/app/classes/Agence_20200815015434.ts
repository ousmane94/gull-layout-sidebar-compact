import {Comite   } from './Comite';
import {  Quartier } from './Quartier';

export class Agence {
    constructor() {}
     idagence: number;
     codeagence: string;
	 // tslint:disable-next-line:indent
	 adresseagence: string;
	 // tslint:disable-next-line:indent
	 comite: Comite;
	 // tslint:disable-next-line:indent
	 quartier: Quartier;
	 
}