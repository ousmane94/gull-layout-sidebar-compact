import {  Agence } from './Agence';
import {  Profils } from './Profils';

export class Utilisateur {
    constructor() {}
     idutilisateur: number;
     agence: Agence;
     profils: Profils;

}
