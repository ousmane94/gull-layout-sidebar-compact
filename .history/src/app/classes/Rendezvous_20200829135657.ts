import {Pays   } from './Pays';
export class Rendezvous {
    constructor() {}
     idrendezvous: number;
     numerorendezvous: string;
     heurerendezvous: string;
     numeromembre: string;
     montant: number;
     descriptionrendezvous: string;
     etat: number;
}
