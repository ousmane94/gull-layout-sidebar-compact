import {Pays   } from './Pays';
import { Planification } from './Planification';
import { Utilisateur } from './Utilisateur';
export class Rendezvous {
    constructor() {}
     idrendezvous: number;
     numerorendezvous: string;
     heurerendezvous: string;
     numeromembre: string;
     montant: number;
     descriptionrendezvous: string;
     etat: number;
     planification: Planification;
     utilisateur: Utilisateur;
}
