import {Pays   } from './Pays';
export class Region {
    constructor() {}
     idregion: number;
     nomregion: string;
     pays: Pays;
}
