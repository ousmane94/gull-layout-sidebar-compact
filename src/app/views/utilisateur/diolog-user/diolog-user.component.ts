import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AgenceService } from 'src/app/shared/services/agence.service';
interface DialogData {
  action?: any;
  utilisateur?: any;
}
@Component({
  selector: 'app-diolog-user',
  templateUrl: './diolog-user.component.html',
  styleUrls: ['./diolog-user.component.scss']
})
export class DiologUserComponent implements OnInit {
  agence: any;
  profils: any;
  loading: boolean;
  loadingText: string;
  regionForm: FormGroup;
  categories;
  totalElements;
  data: DialogData;
  utilisateur; urictg;  urictgagence; urictgprofil; monprofils;
  agenceSelect: any;
  profilsSelect: any;


  // tslint:disable-next-line:max-line-length
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private toastr: ToastrService, private userService: UserService, private userservice: UserService, private agenceService: AgenceService) { }


  ngOnInit() {
    this.utilisateur = this.data.utilisateur;
    console.log('notre user', this.data.utilisateur);
    // tslint:disable-next-line:triple-equals
    if ( this.utilisateur == '') {
      this.urictgprofil = '';
    } else {
      console.log('regarde profils', this.utilisateur);
      this.urictgprofil = this.utilisateur.profils.idprofils;
      this.monprofils = this.utilisateur.profils;
      console.log('sama profils', this.monprofils);
      if ( this.utilisateur.agence == null) {
        this.urictgagence = null;
      } else {

        this.urictg = this.utilisateur.agence.idagence;
        this.urictgagence = this.utilisateur.agence;
        console.log('sama agence', this.urictgagence);
      }
    }
    this.getProfils();
    this.getAgences();
    this.regionForm = this.fb.group({
      idagence: [this.urictg, Validators.required],
      idprofils: [this.urictgprofil, Validators.required],
      idutilisateur: [this.utilisateur.idutilisateur, Validators.required],
      nom: [this.utilisateur.nom, Validators.required],
      prenom: [this.utilisateur.prenom, Validators.required],
      matricule: [this.utilisateur.matricule, Validators.required],
      email: [this.utilisateur.email, Validators.required],
      telephone: [this.utilisateur.telephone, Validators.required],
      password: [this.utilisateur.password, Validators.required],
      etat: [this.utilisateur.etat, Validators.required]


    });
  }
  getProfils() {
    this.userService.listeProfils()
      .subscribe(data => {
       this.profils = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  getAgences() {
    this.agenceService.listeAgenceActive()
      .subscribe(data => {
       this.agence = data; // on charge les données
       }
       , err => { console.log(err); });

  }
  closeModal() {
    this.activeModal.close({ confirmed: false });
  }
  getCommuneSelect(idcommuneSelect) {
    // tslint:disable-next-line:triple-equals
    this.agenceSelect = this.agence.find(agence => agence.idagence == idcommuneSelect.target.value);
  }
  getUserSelect(idutilisateurSelect) {
    // tslint:disable-next-line:triple-equals
    this.profilsSelect = this.profils.find(profils => profils.idprofils == idutilisateurSelect.target.value);
  }
  saveRegion() {
    // this.regionForm.value.state = s(this.regionForm.value.state);
    this.loading = true;
    this.regionForm.value.profils = this.profilsSelect;
    this.regionForm.value.agence = this.agenceSelect;

    const data = {
      agence: this.agenceSelect,
      idutilisateur: this.regionForm.value.idutilisateur,
      nom: this.regionForm.value.nom,
      prenom: this.regionForm.value.prenom,
      matricule: this.regionForm.value.matricule,
      telephone: this.regionForm.value.telephone,
      password: this.regionForm.value.password,
      email: this.regionForm.value.email,
      profils: this.profilsSelect,
      etat: this.regionForm.value.etat

      };

    this.loadingText = 'Enregistrement en cours...';
    if (this.data.action === 'add') {
      console.log('save Utilisateur', data);

       this.userService.saveutilisateur(data)
        .subscribe(res => {
           this.loading = false;
           this.activeModal.close({ confirmed: true });
           this.toastr.success('Utilisateur enregistré avec succés!', 'Succes!', { timeOut: 5000 });

         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });
           }
         );
    } else {
      if ( data.agence == null) {
        data.agence = this.urictgagence;
      }
      if ( data.profils == null) {
        data.profils = this.monprofils;
      }
      console.log('*****************', data);

       this.userService.updateUtilisateur(this.regionForm.value.idutilisateur, data)
        .subscribe(res => {
           this.loading = false;
           this.toastr.success('Utilisateur modifié avec succés!', 'Succes!', { timeOut: 5000 });
           this.activeModal.close({ confirmed: true });
         },
           error => {
             console.log('test', error);
             this.loading = false;
             this.toastr.error('Une erreur est survenue!', 'Erreur!', { timeOut: 5000 });

           }
         );
    }


  }
}
