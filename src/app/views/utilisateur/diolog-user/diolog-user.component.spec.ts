import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiologUserComponent } from './diolog-user.component';

describe('DiologUserComponent', () => {
  let component: DiologUserComponent;
  let fixture: ComponentFixture<DiologUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiologUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiologUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
