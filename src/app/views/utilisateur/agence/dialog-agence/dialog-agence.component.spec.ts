import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAgenceComponent } from './dialog-agence.component';

describe('DialogAgenceComponent', () => {
  let component: DialogAgenceComponent;
  let fixture: ComponentFixture<DialogAgenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAgenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
