import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogComiteComponent } from './dialog-comite.component';

describe('DialogComiteComponent', () => {
  let component: DialogComiteComponent;
  let fixture: ComponentFixture<DialogComiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogComiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogComiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
