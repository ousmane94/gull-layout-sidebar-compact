import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogPaysComponent } from '../dialog-pays/dialog-pays.component';
import { PaysService } from 'src/app/shared/services/pays.service';

@Component({
  selector: 'app-liste-pays',
  templateUrl: './liste-pays.component.html',
  styleUrls: ['./liste-pays.component.scss']
})
export class ListePaysComponent implements OnInit {
pays: any ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private paysService: PaysService) { }


  ngOnInit() {
    this.getPays();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(DialogPaysComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      pays: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getPays();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(pays) {
    const dialogRef = this.modalService.open(DialogPaysComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      pays: pays,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getPays();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getPays() {
    this.paysService.listepays()
      .subscribe(data => {
       this.pays = data; // on charge les données
       }
       , err => { console.log(err); });

  }
}
