import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPaysComponent } from './dialog-pays.component';

describe('DialogPaysComponent', () => {
  let component: DialogPaysComponent;
  let fixture: ComponentFixture<DialogPaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
