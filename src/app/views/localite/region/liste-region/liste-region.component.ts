import { Component, OnInit } from '@angular/core';
import { DialogRegionComponent } from '../dialog-region/dialog-region.component';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PaysService } from 'src/app/shared/services/pays.service';

@Component({
  selector: 'app-liste-region',
  templateUrl: './liste-region.component.html',
  styleUrls: ['./liste-region.component.scss']
})
export class ListeRegionComponent implements OnInit {

  regions ;
constructor(private toastr: ToastrService, private modalService: NgbModal, private paysService: PaysService) { }


  ngOnInit() {
    this.getRegion();
  }
  goToDialog() {
    const dialogRef = this.modalService.open(DialogRegionComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'add',
      region: '',
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          console.log(res);
          this.getRegion();

        }
      }).catch(e => {
        console.log(e);
      });
  }
  goToDialogUpdate(regions) {
    console.log('sama udate region', regions);
    const dialogRef = this.modalService.open(DialogRegionComponent, {centered: true, size: 'lg'});
    dialogRef.componentInstance.data = {
      action: 'edit',
      region: regions,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }

        if (res.confirmed) {
          this.getRegion();
        }
      }).catch(e => {
        console.log(e);
      });
  }
  getRegion() {
    this.paysService.listeRegions()
      .subscribe(data => {
       this.regions = data; // on charge les données
       }
       , err => { console.log(err); });

    /* this.regions = [{
      idregion : 1,
      nomregion : 'Dakar',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }, {
 idregion : 2,
      nomregion : 'Saint-louis',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    },
    {
      idregion : 3,
      nomregion : 'Thies',
      pays: {
        idpays : 1,
        nompays: 'Senegal',
        statut: 1
      },
      statut: 1
    }
  ]; */
  }

}
