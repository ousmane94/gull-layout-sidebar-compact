import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRegionComponent } from './dialog-region.component';

describe('DialogRegionComponent', () => {
  let component: DialogRegionComponent;
  let fixture: ComponentFixture<DialogRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
