import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogQuartierComponent } from './dialog-quartier.component';

describe('DialogQuartierComponent', () => {
  let component: DialogQuartierComponent;
  let fixture: ComponentFixture<DialogQuartierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogQuartierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogQuartierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
