import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCommuneComponent } from './dialog-commune.component';

describe('DialogCommuneComponent', () => {
  let component: DialogCommuneComponent;
  let fixture: ComponentFixture<DialogCommuneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCommuneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCommuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
