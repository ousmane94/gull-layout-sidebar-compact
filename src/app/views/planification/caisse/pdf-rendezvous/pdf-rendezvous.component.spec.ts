import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfRendezvousComponent } from './pdf-rendezvous.component';

describe('PdfRendezvousComponent', () => {
  let component: PdfRendezvousComponent;
  let fixture: ComponentFixture<PdfRendezvousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfRendezvousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
