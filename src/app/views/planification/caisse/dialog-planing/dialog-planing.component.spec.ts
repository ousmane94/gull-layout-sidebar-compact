import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPlaningComponent } from './dialog-planing.component';

describe('DialogPlaningComponent', () => {
  let component: DialogPlaningComponent;
  let fixture: ComponentFixture<DialogPlaningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPlaningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPlaningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
