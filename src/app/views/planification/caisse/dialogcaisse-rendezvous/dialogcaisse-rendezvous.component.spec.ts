import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogcaisseRendezvousComponent } from './dialogcaisse-rendezvous.component';

describe('DialogcaisseRendezvousComponent', () => {
  let component: DialogcaisseRendezvousComponent;
  let fixture: ComponentFixture<DialogcaisseRendezvousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogcaisseRendezvousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogcaisseRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
