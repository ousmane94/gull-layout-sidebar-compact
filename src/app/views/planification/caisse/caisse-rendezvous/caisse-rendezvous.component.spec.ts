import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaisseRendezvousComponent } from './caisse-rendezvous.component';

describe('CaisseRendezvousComponent', () => {
  let component: CaisseRendezvousComponent;
  let fixture: ComponentFixture<CaisseRendezvousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaisseRendezvousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaisseRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
