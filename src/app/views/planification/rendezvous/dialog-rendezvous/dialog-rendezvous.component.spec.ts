import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRendezvousComponent } from './dialog-rendezvous.component';

describe('DialogRendezvousComponent', () => {
  let component: DialogRendezvousComponent;
  let fixture: ComponentFixture<DialogRendezvousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogRendezvousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
