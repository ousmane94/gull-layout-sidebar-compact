import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddplanificationComponent } from './addplanification/addplanification.component';
import { ForgotComponent } from '../sessions/forgot/forgot.component';
import { EditeplanificationComponent } from './editeplanification/editeplanification.component';
import { ListeplanificationComponent } from './listeplanification/listeplanification.component';
import { ListRendezvousComponent } from './rendezvous/list-rendezvous/list-rendezvous.component';
import { CaisseRendezvousComponent } from './caisse/caisse-rendezvous/caisse-rendezvous.component';
import { ListPlaningComponent } from './caisse/list-planing/list-planing.component';
import { CreateComponent } from './rendezvous/create/create.component';
import { LitRendezvousComponent } from './credit/lit-rendezvous/lit-rendezvous.component';


const routes: Routes = [
  {
    path: 'rendezvous',
    component: ListRendezvousComponent
  },
  {
    path: 'rendezvousV2',
    component: CreateComponent
  },
  {
    path: 'modifier',
    component: EditeplanificationComponent
  },
  {
    path: 'liste',
    component: ListeplanificationComponent
  },
  {
    path: 'caisse/rendezvous',
    component: CaisseRendezvousComponent
  },
  {
    path: 'caisse/planing',
    component: ListPlaningComponent
  },
  {
    path: 'credit/rendezvous',
    component: LitRendezvousComponent
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanificationRoutingModule { }
