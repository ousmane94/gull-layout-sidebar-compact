import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditeplanificationComponent } from './editeplanification.component';

describe('EditeplanificationComponent', () => {
  let component: EditeplanificationComponent;
  let fixture: ComponentFixture<EditeplanificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditeplanificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditeplanificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
