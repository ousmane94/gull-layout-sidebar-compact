import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiaRendezvousComponent } from './dia-rendezvous.component';

describe('DiaRendezvousComponent', () => {
  let component: DiaRendezvousComponent;
  let fixture: ComponentFixture<DiaRendezvousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaRendezvousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiaRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
