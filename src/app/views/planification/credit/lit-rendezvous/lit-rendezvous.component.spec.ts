import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LitRendezvousComponent } from './lit-rendezvous.component';

describe('LitRendezvousComponent', () => {
  let component: LitRendezvousComponent;
  let fixture: ComponentFixture<LitRendezvousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LitRendezvousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LitRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
