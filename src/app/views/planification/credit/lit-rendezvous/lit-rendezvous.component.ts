import { Component, OnInit } from '@angular/core';
import { DiaRendezvousComponent } from '../dia-rendezvous/dia-rendezvous.component';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlaningService } from 'src/app/shared/services/planing.service';
import { LocalStoreService } from 'src/app/shared/services/local-store.service';

@Component({
  selector: 'app-lit-rendezvous',
  templateUrl: './lit-rendezvous.component.html',
  styleUrls: ['./lit-rendezvous.component.scss']
})
export class LitRendezvousComponent implements OnInit {
  rendezvous: any ;
  codematricule: string = this.strore.getItem('matricule');
  constructor(private toastr: ToastrService, private modalService: NgbModal,
    private planing: PlaningService,
    private strore: LocalStoreService
    ) { }

    ngOnInit() {
      this.getRendezvous();
    }
    goToDialog() {
      const dialogRef = this.modalService.open(DiaRendezvousComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'add',
        rendezvous: '',
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getRendezvous();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    goToDialogUpdate(rendezvous) {
      console.log('edit rendez--------', rendezvous);
      const dialogRef = this.modalService.open(DiaRendezvousComponent, {centered: true, size: 'lg'});
      dialogRef.componentInstance.data = {
        action: 'edit',
        rendezvous: rendezvous,
      };
      dialogRef.result
        .then((res) => {
          if (!res) {
            return;
          }
          if (res.confirmed) {
            this.getRendezvous();
          }
        }).catch(e => {
          console.log(e);
        });
    }
    getRendezvous() {
      this.planing.listrendezvousAgentCredit(this.codematricule)
        .subscribe(data => {
         this.rendezvous = data; // on charge les données
         console.log('liste rendez--------', this.rendezvous);
         }
         , err => { console.log(err); });
    }
 }
