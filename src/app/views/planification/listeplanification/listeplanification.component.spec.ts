import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeplanificationComponent } from './listeplanification.component';

describe('ListeplanificationComponent', () => {
  let component: ListeplanificationComponent;
  let fixture: ComponentFixture<ListeplanificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeplanificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeplanificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
