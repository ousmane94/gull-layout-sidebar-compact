import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/classes/Utilisateur';
import { Comite } from 'src/app/classes/Comite';

import { Observable } from 'rxjs';
import { Profils } from 'src/app/classes/Profils';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  jwt: string;
  constructor(private authentification: AuthService, private httpclient: HttpClient , private router: Router) {

  }


   // ajouter utilisateur
  public saveutilisateur(utilisateur): Observable<Utilisateur> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Utilisateur>(this.authentification.adresse + '/meczy/utilisateur/create', utilisateur, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
 // ajouter utilisateur a un comité
  public ajouterUtilisateurComite(usercomite) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/utilisateur/comite/ajout', usercomite, {headers: new HttpHeaders({'authorization': this.jwt})});
  }



  // modification utilisateur
  public updateUtilisateur(idutilisateur: any, utilisateur) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/utilisateur/update/' + idutilisateur, utilisateur, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // liste Utilisateur pour admin
   listeUtilisateur() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Utilisateur>( this.authentification.adresse + '/meczy/utilisateur/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }

  // liste agent de credit par agence
   listeAgentCredit(codeagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Utilisateur>( this.authentification.adresse + '/meczy/utilisateur/agence/credit/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

    // liste agent de credit par agence
    listeAgentCreditAll() {
      if (this.jwt == null) {
        this.jwt = localStorage.getItem('token');
      }
      // tslint:disable-next-line:max-line-length
      return this.httpclient.get<Utilisateur>( this.authentification.adresse + '/meczy/utilisateur/agent/credit', {headers: new HttpHeaders({'authorization': this.jwt})});
    }

  // liste agent  superviseur par agence
   listeAgentSuperviseur(codeagence) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Utilisateur>( this.authentification.adresse + '/meczy/utilisateur/agence/superviseur/' + codeagence, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
    // utilisateur  Connecté
   getUtilisateurConnecte(matricule) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Utilisateur>( this.authentification.adresse + '/meczy/utilisateur/connecte/' + matricule, {headers: new HttpHeaders({'authorization': this.jwt})});
  }



 // pour la utilisateur
  public deleteRessource(idutilisateur) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/utilisateur/delete/' + idutilisateur, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

// ajouter Comite
  public saveComite(comite): Observable<Comite> {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.post<Comite>(this.authentification.adresse + '/meczy/comite/create', comite, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
  // modification comite
  public updateComite(idcomite: any, comite: Comite) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.put(this.authentification.adresse + '/meczy/comite/update/' + idcomite, comite, {headers: new HttpHeaders({'authorization': this.jwt})});
  }
// pour la suppression comite
  public deleteComite(idcomite) {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.delete(this.authentification.adresse + '/meczy/comite/delete/' + idcomite, {headers: new HttpHeaders({'authorization': this.jwt})});
  }

    // liste comite
  listeComite() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Comite>( this.authentification.adresse + '/meczy/comite/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }
    // liste comite
    listeComiteActive() {
      if (this.jwt == null) {
        this.jwt = localStorage.getItem('token');
      }
      // tslint:disable-next-line:max-line-length
      return this.httpclient.get<Comite>( this.authentification.adresse + '/meczy/comite/active/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
    }

  // liste Profils
  listeProfils() {
    if (this.jwt == null) {
      this.jwt = localStorage.getItem('token');
    }
    // tslint:disable-next-line:max-line-length
    return this.httpclient.get<Profils>( this.authentification.adresse + '/meczy/profils/liste', {headers: new HttpHeaders({'authorization': this.jwt})});
  }




}
